# Installation

The project uses poetry

First clone this repository,
then cd to the folder created

it should be

```cd foobartory-production-line```

If needed, install poetry by typing

```pip install poetry```

then run 

```poetry update```

in order to install all dependencies

and then run the program by typing

```poetry run foobartory```

# Limitations

- The time lost on change of workspace has not been coded,
- An action affects only one resource even if multiple resources could be affected (5 for example for the sell_foobar action)
- probably some others that I don't think about now/

# Configuration

I have spent much more time than planned (and asked) on this project that documentation, testing and online comments are quite scarce tbh.

That said, now could be the time of configuring it, enhance logs and test come strategies.

In order to do that, easiest way is to copy-paste the yaml file in the Strategies subfolder.



```yaml
actions:
  preferred:
    # For now, I might not have time to code the resources part but it would be nicer
    resources:
      foo: 3
      bar: 3
    action: [buy_new_robot_action,sell_foobar_action]
  default:
    pattern: [3*mine_foo_action,3*mine_bar_action,forge_foobar_action]

# Possible actions are
# - idle_action
# - mine_foo_action
# - mine_bar_action
# - forge_foobar_action
# - sell_foobar_action
# - buy_new_robot_action

```

the pattern option works this way :
- the [] means the actions inside the list will be repeated ad vitam eternam
- if an action comes in the form 3*action, the action will be repeated 3 times (always if resources allow that)