from dataclasses import dataclass
from itertools import cycle
from pathlib import Path
from typing import Any, List, Optional

import yaml


@dataclass
class Strategy:
    preferred_action: Optional[List[str]] = None
    list_actions: Any = None

    def load_strategy_from_yaml_file(self, path: Path):
        with open(path, "r") as f:
            strategy_dict = yaml.safe_load(f.read())

        if "preferred" in strategy_dict["actions"].keys():
            self.preferred_action = strategy_dict["actions"]["preferred"]["action"]

        pat = strategy_dict["actions"]["default"]["pattern"]
        if isinstance(pat, list):
            list_actions = []
            for actions in pat:
                try:
                    nb, action = actions.split("*")
                except ValueError:
                    nb = 1
                    action = actions
                list_actions.extend(int(nb) * [action])
        self.list_actions = cycle(list_actions)
