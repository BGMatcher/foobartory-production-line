from dataclasses import dataclass
from random import randint
from typing import Optional

from foobar.resources import (
    Cost,
    Gain,
    one_bar_gain,
    one_foo_gain,
    one_foobar_gain,
    one_money_gain,
    one_robot_gain,
)


@dataclass
class Action:
    name: str
    cost: Cost
    gain: Optional[Gain] = None
    success_rate: float = 1

    def resources_needed(self):
        if self.name in ["Idle", "Mine Foo", "Forge FooBar", "Sell FooBar", "Buy Robot"]:
            return self.cost
        elif self.name == "Mine Bar":
            time_consumed = randint(5, 20) * 100
            return Cost(time=time_consumed)
        else:
            # Something unexpected happened here
            # use sentry probably
            pass

    def check_income(self):
        if self.name in ["Idle", "Mine Foo", "Mine Bar", "Buy Robot"]:
            return self.gain
        elif self.name == "Forge FooBar":
            if randint(1, 100) <= self.success_rate:
                # Success
                return one_foobar_gain
            else:
                return one_bar_gain
        elif self.name == "Sell FooBar":
            # Special treatment because we might sell more than one robot
            return self.gain


all_actions = {
    "idle_action": Action("Idle", Cost(time=0), Gain()),
    "mine_foo_action": Action("Mine Foo", Cost(time=1000, money=0, foo=0), gain=one_foo_gain),
    "mine_bar_action": Action("Mine Bar", Cost(time=[], money=0, foo=0), gain=one_bar_gain),
    "forge_foobar_action": Action(
        "Forge FooBar",
        Cost(time=2000, money=0, foo=1, bar=1),
        success_rate=60,
    ),
    "sell_foobar_action": Action(
        "Sell FooBar", Cost(time=10000, money=0, foobar=1), gain=one_money_gain
    ),
    "buy_new_robot_action": Action("Buy Robot", Cost(time=0, money=3, foo=6), gain=one_robot_gain),
}
