from dataclasses import dataclass
from typing import List, Union


@dataclass
class Resources:
    money: int = 0
    foo: int = 0
    bar: int = 0
    foobar: int = 0
    robot: int = 0

    def adjust(self, gain_loss_dict):
        if "Gain" in gain_loss_dict.keys():
            self.money = self.money + gain_loss_dict["Gain"].money
            self.foo = self.foo + gain_loss_dict["Gain"].foo
            self.bar = self.bar + gain_loss_dict["Gain"].bar
            self.foobar = self.foobar + gain_loss_dict["Gain"].foobar
            self.robot = self.robot + gain_loss_dict["Gain"].robot
        if "Cost" in gain_loss_dict.keys():
            self.money = self.money - gain_loss_dict["Cost"].money
            self.foo = self.foo - gain_loss_dict["Cost"].foo
            self.bar = self.bar - gain_loss_dict["Cost"].bar
            self.foobar = self.foobar - gain_loss_dict["Cost"].foobar


@dataclass
class Cost:
    time: Union[int, List[float]] = 0
    money: int = 0
    foo: int = 0
    bar: int = 0
    foobar: int = 0


@dataclass()
class Gain(Resources):
    pass


one_foo_gain = Gain(foo=1)
one_bar_gain = Gain(bar=1)
one_foobar_gain = Gain(foobar=1)
one_robot_gain = Gain(robot=1)
one_money_gain = Gain(money=1)
one_foo_one_bar_cost = Cost(foo=1, bar=1)
