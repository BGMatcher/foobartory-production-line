import logging
from dataclasses import dataclass, field
from pathlib import Path
from typing import Dict, Optional

from .actions import all_actions
from .config import CONST_NB_ROBOTS_START, EMERGENCY_EXIT
from .resources import Resources
from .robots import Robot
from .strategy import Strategy


@dataclass
class ProductionLine:
    strategy_path: Path
    number_robots: int = 0
    current_money: int = 0
    # I don't like that but I guess a number must be assigned to every robot we have
    # The idea would be to have multiple strategies depending on the robot's number
    our_robots: Optional[Dict[int, Robot]] = field(default_factory=dict)
    resources = Resources()
    current_time_elapsed: float = 0
    strategy: Strategy = field(default_factory=Strategy)

    def __post_init__(self):
        self.strategy.load_strategy_from_yaml_file(self.strategy_path)
        self.add_robots(CONST_NB_ROBOTS_START)

    def add_robots(self, number_of_robots: int):
        for _ in range(number_of_robots):
            new_index_robot = self.number_robots + 1
            self.our_robots[new_index_robot] = Robot()
            self.number_robots += 1
            self.our_robots[new_index_robot].pattern = self.strategy.list_actions

    def _can_afford(self, action_name: str) -> bool:
        cost = all_actions[action_name].cost
        logging.debug(f"Does action {action_name} can be afforded ?")
        can_be_afforded: bool = False
        if (
            self.resources.foo >= cost.foo
            and self.resources.bar >= cost.bar
            and self.resources.money >= cost.money
            and self.resources.foobar >= cost.foobar
        ):
            can_be_afforded = True
        logging.debug(f"The answer is: {'yes' if can_be_afforded else 'NO !! Go back to work'}")
        return can_be_afforded

    def decide_next_action(self, id_robot: int) -> bool:
        robot = self.our_robots[id_robot]
        for optimal in self.strategy.preferred_action:
            if self._can_afford(optimal):
                robot.program(optimal)
                resources_needed = all_actions[optimal].resources_needed()
                self._pay_price(resources_needed)
                robot.current_action_will_be_over = robot.current_lifetime + resources_needed.time
                return True
        # We should make sure there is a possible action in here or else we will loop forever
        i = 0
        while i < EMERGENCY_EXIT:
            next_action = next(robot.pattern)
            if self._can_afford(next_action):
                robot.program(next_action)
                resources_needed = all_actions[next_action].resources_needed()
                self._pay_price(resources_needed)
                robot.current_action_will_be_over = robot.current_lifetime + resources_needed.time
                return True
            i += 1
        return False

    def _pay_price(self, resources: Resources):
        self.resources.adjust({"Cost": resources})

    def age_robots(self):
        for r in self.our_robots:
            r.age()

    def __str__(self):
        pl_str = "Production Line:\nOur robots so far:\n"
        for k in self.our_robots.keys():
            pl_str += f"\tRobot #{k:02d}: {self.our_robots[k]}\n"
        nb = len(self.our_robots)
        pl_str += f"for a quite impressive number of {nb} robots so far\n"
        if nb < 10:
            pl_str += "Ok, maybe not so impressive YET\n"
        pl_str += f"Our resources so far:\n{self.resources}"
        return pl_str
