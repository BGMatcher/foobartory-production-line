import logging
from dataclasses import dataclass, field
from itertools import cycle
from typing import Optional

import randomname

from foobar.actions import Action, all_actions
from foobar.config import CONST_TIMELAPSE
from foobar.resources import Resources


@dataclass
class Robot:
    name: str = field(default_factory=randomname.get_name)
    current_action: Action = all_actions["idle_action"]
    current_action_will_be_over: int = 0
    current_lifetime: int = 0
    pattern: Optional[cycle] = None

    def age(self, timelapse=CONST_TIMELAPSE) -> Optional[Resources]:
        self.current_lifetime += timelapse

    def is_idle(self):
        return self.current_action == all_actions["idle_action"]

    def finishes_an_action(self) -> bool:
        return self.current_lifetime >= self.current_action_will_be_over

    def get_reward(self):
        return self.current_action.check_income()

    def program(self, action_name: str):
        self.current_action = all_actions[action_name]
        logging.info(f"Robot {self.name} has been programmed with action {action_name}")
        logging.info(f"Action will be finished at t={self.current_action_will_be_over}")

    def __str__(self):
        if self.current_action == all_actions["idle_action"]:
            return f"{self.name:30} - Could work harder"
        else:
            return f"{self.name:30} - {self.current_action.name}"
