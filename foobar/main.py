import logging
from itertools import count
from pathlib import Path

from .config import CONST_NB_ROBOTS_START, CONST_TIMELAPSE
from .production_line import ProductionLine

brilliant_strategy = "strategy1"

logging.basicConfig(level=logging.INFO)


def start():

    logging.info("Starting our production line")
    logging.info(f"with {CONST_NB_ROBOTS_START} robots")

    strategy_path = (
        Path(__file__).parents[1].resolve() / "Strategies" / f"{brilliant_strategy}.yaml"
    )

    our_line = ProductionLine(strategy_path=strategy_path)
    logging.info(f"Strategy applied: \n[={our_line.strategy}")
    logging.info(our_line)

    for t in count(start=0, step=CONST_TIMELAPSE):
        if our_line.resources.robot >= 30:
            logging.info(
                f"Program stopped after {t/1000} seconds elapsed. We have our 30 robots. Yay us"
            )
            break

        robots_added_this_round: int = 0
        for idx, robot in our_line.our_robots.items():
            if robot.is_idle():
                our_line.decide_next_action(idx)
            elif robot.finishes_an_action():
                logging.info(f"action finished for robot {robot.name}")
                gain = robot.get_reward()
                robots_added_this_round += gain.robot
                logging.debug(f"Gain: {gain}")
                our_line.resources.adjust({"Gain": gain})
                logging.debug(f"New line resources: {our_line.resources}")
                our_line.decide_next_action(idx)
            robot.age()
        if robots_added_this_round:
            our_line.add_robots(robots_added_this_round)


if __name__ == "__main__":
    start()
